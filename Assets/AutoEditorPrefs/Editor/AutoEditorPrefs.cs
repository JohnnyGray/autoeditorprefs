﻿using UnityEditor;
using UnityEngine;
using System.IO;

[InitializeOnLoad]
public static class AutoEditorPrefs
{
	static AutoEditorPrefsScriptableObject m_cAutoEditorPrefsScriptableObject = null;

	static AutoEditorPrefs()
	{
		GetAutoEditorPrefsScriptableObjectReference();
	}

	static void GetAutoEditorPrefsScriptableObjectReference()
	{
		if (m_cAutoEditorPrefsScriptableObject == null)
		{
			string[] astrAutoEditorPrefsScriptableObjectGuids = AssetDatabase.FindAssets("t:AutoEditorPrefsScriptableObject");

			if (astrAutoEditorPrefsScriptableObjectGuids.Length > 0)
			{
				string strAutoEditorPrefsScriptableObjectPath = AssetDatabase.GUIDToAssetPath(astrAutoEditorPrefsScriptableObjectGuids[0]);

				m_cAutoEditorPrefsScriptableObject = AssetDatabase.LoadAssetAtPath<AutoEditorPrefsScriptableObject>(strAutoEditorPrefsScriptableObjectPath);
			}
			else
			{
				m_cAutoEditorPrefsScriptableObject = ScriptableObject.CreateInstance<AutoEditorPrefsScriptableObject>();

				Directory.CreateDirectory(Application.dataPath + "/AutoEditorPrefs/Settings/");

				AssetDatabase.CreateAsset(m_cAutoEditorPrefsScriptableObject, "Assets/AutoEditorPrefs/Settings/AutoEditorPrefsScriptableObject.asset");
				AssetDatabase.SaveAssets();
			}
		}
	}

	public static void Save()
	{
		EditorUtility.SetDirty(m_cAutoEditorPrefsScriptableObject);
		AssetDatabase.SaveAssets();
	}

	public static void SetBool(string strKey, bool bValue)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.SetBool(strKey, bValue);
	}

	public static bool GetBool(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		return m_cAutoEditorPrefsScriptableObject.GetBool(strKey);
	}

	public static void DeleteBool(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.DeleteBool(strKey);
	}

	public static void SetInt(string strKey, int nValue)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.SetInt(strKey, nValue);
	}

	public static int GetInt(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		return m_cAutoEditorPrefsScriptableObject.GetInt(strKey);
	}

	public static void DeleteInt(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.DeleteInt(strKey);
	}

	public static void SetFloat(string strKey, float fValue)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.SetFloat(strKey, fValue);
	}

	public static float GetFloat(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		return m_cAutoEditorPrefsScriptableObject.GetFloat(strKey);
	}

	public static void DeleteFloat(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.DeleteFloat(strKey);
	}

	public static void SetString(string strKey, string strValue)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.SetString(strKey, strValue);
	}

	public static string GetString(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		return m_cAutoEditorPrefsScriptableObject.GetString(strKey);
	}

	public static void DeleteString(string strKey)
	{
		GetAutoEditorPrefsScriptableObjectReference();

		m_cAutoEditorPrefsScriptableObject.DeleteString(strKey);
	}
}
